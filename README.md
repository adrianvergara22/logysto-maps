**Logysto-Server**

Backend desarrollado con node utilizando express + typescript + jwt + typeOrm.

**PRE-REQUISITOS**

1. Node la versión LTS `https://nodejs.org/es/`


2. Instalar TypeScript de manera global. Para esto abren una consola en modo administrador para windows y
   ejecutan `npm install -g typescript` para usuarios linux o mac deben ejecutar `sudo npm install -g typescript`


3. instalación de manera global de nodemon. Hacen lo mismo que en el paso 2 y ejecutan `npm install -g nodemon` para
   linux y mac `sudo npm install -g nodemon`

**INSTALACIÓN Y PUESTA EN MARCHA**

1. Clonar el repositorio `git clone https://gitlab.com/adrianvergara22/logysto-maps.git`


2. Ejecutar el comando `npm install`


3. Crear la base de datos llamada db_logysto, si quieren modificar el nombre, primero deben modificarlo en el
   fichero `.env` y `ormconfig.ts`


4. La cadena de conexión de la base de datos debe ser modificada en 2 ficheros `.env` y `ormconfig.ts`


5. Correr las migraciones ejecutando el comando `migration:run`


6. Ejecutar el comando `tsc` para generar el directorio `dist` y luego ejecutar el comando `node dist/app.js`


**NOTA:**
En la raíz del proyecto hay un fichero llamado `logysto.postman_collection.json`. Este fichero debe ser importado en postman para visualizar los endpoints documentados.
