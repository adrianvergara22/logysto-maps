import {Request, Response} from "express";
import UserService from "../services/user.service";
import {jsonApiResponse} from "../services/json-api-response.service";


class UserController {

    static userService = new UserService();

    static getAll = async (req: Request, res: Response) => {
        return jsonApiResponse(res, await UserController.userService.getAll());
    }

    static create = async (req: Request, res: Response) => {
        const {body} = req;
        return jsonApiResponse(res, await UserController.userService.create(body));
    }
}

export default UserController;
