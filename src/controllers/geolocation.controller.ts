import {Request, Response} from "express";
import {jsonApiResponse} from "../services/json-api-response.service";
import {GeolocationService} from "../services/geolocation.service";


class GeolocationController {

    static geolocationService = new GeolocationService();

    static getLocationByAddress = async (req: Request, res: Response) => {
        const {address} = req.params;
        return jsonApiResponse(res, await GeolocationController.geolocationService.getLocationByAddress(address));
    }

}

export default GeolocationController;
