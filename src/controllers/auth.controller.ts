import {Request, Response} from "express";
import {jsonApiResponse} from "../services/json-api-response.service";
import AuthService from "../services/auth.service";


class AuthController {

    static authService = new AuthService();

    static authenticate = async (req: Request, res: Response) => {
        const {body} = req;
        return jsonApiResponse(res, await AuthController.authService.authenticate(body));
    }

    static refreshToken = async (req: Request, res: Response) => {
        const refresh = <string>req.get("refreshToken");
        return jsonApiResponse(res, await AuthController.authService.refreshToken(refresh));
    }

}

export default AuthController;
