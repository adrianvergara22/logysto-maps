import {Router} from "express";
import {authMiddleware} from "../middlewares/auth.middleware";
import GeolocationController from "../controllers/geolocation.controller";

const router = Router();

router.get('/address/:address', authMiddleware, GeolocationController.getLocationByAddress);

export default router;
