import {Router} from "express";
import {validationMiddleware} from "../middlewares/validation.middleware";
import {validateAuth} from "../validations/auth.validation";
import AuthController from "../controllers/auth.controller";

const router = Router();

router.post('/', validateAuth, validationMiddleware, AuthController.authenticate);
router.post('/refresh-token', AuthController.refreshToken);

export default router;
