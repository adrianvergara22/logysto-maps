import {Router} from "express";
import UserController from "../controllers/user.controller";
import {authMiddleware} from "../middlewares/auth.middleware";
import {validationMiddleware} from "../middlewares/validation.middleware";
import {validateCreate} from "../validations/user.validation";

const router = Router();

router.get('/', authMiddleware, UserController.getAll);
router.post('/', validateCreate, validationMiddleware, UserController.create);

export default router;
