import "reflect-metadata";
import {createConnection} from "typeorm";
import {User} from "../entities/user";
import config from "../config/config";

export default class Connection {

    constructor() {
        this.initDb();
    }

    private initDb() {
        createConnection({
            type: "mysql",
            host: config.dbHost,
            port: config.dbPort,
            username: config.dbUsername,
            password: config.dbPassword,
            database: config.dbName,
            entities: [
                User
            ],
            synchronize: false,
            logging: true
        }).then(connection => {
            console.log("Base de datos conectada");
        }).catch(error => console.log(error));
    }

}
