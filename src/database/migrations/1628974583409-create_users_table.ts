import {MigrationInterface, QueryRunner} from "typeorm";

export class createUsersTable1628974583409 implements MigrationInterface {
    name = 'createUsersTable1628974583409'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`db_logysto\`.\`users\` (\`id\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(70) NOT NULL, \`last_name\` varchar(70) NULL, \`email\` varchar(80) NOT NULL, \`password\` varchar(80) NOT NULL, \`phone\` varchar(15) NULL, \`address\` varchar(60) NULL, \`created_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), UNIQUE INDEX \`IDX_97672ac88f789774dd47f7c8be\` (\`email\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX \`IDX_97672ac88f789774dd47f7c8be\` ON \`db_logysto\`.\`users\``);
        await queryRunner.query(`DROP TABLE \`db_logysto\`.\`users\``);
    }

}
