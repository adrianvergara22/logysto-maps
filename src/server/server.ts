import express, {Application} from 'express';
import cors from 'cors';
import userRoutes from "../routes/user.route";
import authRoutes from "../routes/auth.route";
import geolocationRoutes from "../routes/geolocation.route";
import Connection from "../database/connection";
import config from "../config/config";

class Server {

    private app: Application;
    private readonly port: string;
    private apiPaths = {
        users: '/api/v1/users',
        auth: '/api/v1/auth',
        geolocations: '/api/v1/geolocations',
    }

    constructor() {
        this.app = express();
        this.port = config.port;
        this.dbConnection();
        this.middlewares();
        this.routes();
    }

    dbConnection() {
        new Connection();
    }

    middlewares() {
        //cors
        this.app.use(cors());
        //read body
        this.app.use(express.json());
        this.app.use(express.urlencoded({extended: true}));
        //public directory
        this.app.use(express.static('public'));
    }

    routes() {
        this.app.use(this.apiPaths.users, userRoutes);
        this.app.use(this.apiPaths.auth, authRoutes);
        this.app.use(this.apiPaths.geolocations, geolocationRoutes);
    }

    listen() {
        this.app.listen(this.port, () => {
            console.log(`Servidor corriendo en puerto ${this.port}`);
        })
    }

}

export default Server;
