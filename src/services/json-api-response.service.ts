import {Response} from "express";

// @ts-ignore
export const jsonApiResponse = (res: Response, {code, success, message, content}) => {
    res.status(code).json({success, message, content})
}

export const successResponse = (message: string, content: any = {}, code: number = 200): any => {
    return {
        code,
        success: true,
        message,
        content
    };
}

export const errorResponse = (code: number, message: string = '', content: any = {}): any => {
    return {
        code,
        success: false,
        message,
        content
    };
}
