export class ValidationService {

    static replaceAll(text: string, find: any[], replace: any[]): string {
        for (let i in find) {
            text = text.replace(find[i], replace[i])
        }
        return text;
    }

    static isEmpty(data: any): Boolean {
        return !(data !== undefined && data !== null && data !== '');
    }


}
