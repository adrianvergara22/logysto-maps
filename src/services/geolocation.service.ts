import {httpStatus} from "../config/http-status";
import config from "../config/config";
import {errorResponse, successResponse} from "./json-api-response.service";
import {Message} from "../config/message";
import {IGeolocation, IGeolocationResponse} from "../interfaces/geolocation.interface";
import {ValidationService} from "./validation.service";

const axios = require('axios').default;

export class GeolocationService {

    private readonly providers: string[];

    constructor() {
        this.providers = [config.googleProvider, config.tomtomProvider];
    }

    async getLocationByAddress(address: string): Promise<IGeolocationResponse> {

        for (let provider of this.providers) {
            const url = this.buildUrl(provider, address);
            let getLocation: any;

            try {
                getLocation = await axios.create().get(url);
            } catch (error) {
                return errorResponse(httpStatus.INTERNAL_SERVER_ERROR, error.message);
            }

            const getResponse: IGeolocation = this.managementResponseByProvider(provider, getLocation.data);
            if (!ValidationService.isEmpty(getResponse.latitude)) {
                return successResponse(Message.locationFound, getResponse);
            }

        }

        return errorResponse(httpStatus.NOT_FOUND, Message.locationNotFound);
    }

    private buildUrl(provider: string, address: string): string {
        return provider === config.googleProvider
            ? encodeURI(`${config.googleMapsUrl}/geocode/json?address=${address}&key=${config.googleMapsApiKey}`)
            : encodeURI(`${config.tomtomUrl}/search/${address}.json?limit=1&key=${config.tomtomApiKey}`);
    }

    private managementResponseByProvider(provider: string, response: any): IGeolocation {
        //Google Provider
        if (provider === config.googleProvider && response?.status === 'OK') {
            const {lat, lng} = response.results[0].geometry.location;
            return {latitude: lat, longitude: lng, provider};
        }

        //Tomtom provider
        if (provider === config.tomtomProvider && response.results.length > 0) {
            const {lat, lon} = response.results[0].position;
            return {latitude: lat, longitude: lon, provider};
        }

        return {latitude: '', longitude: '', provider};
    }

}
