import {getRepository} from "typeorm";
import {User} from "../entities/user";
import bcrypt = require("bcrypt");
import {httpStatus} from "../config/http-status";
import {ICreateUser, IUserResponse} from "../interfaces/user.interface";
import {errorResponse, successResponse} from "./json-api-response.service";
import {Message} from "../config/message";
import {IAuthResponse} from "../interfaces/auth.interface";
import AuthService from "./auth.service";

export default class UserService {


    private authService: AuthService;

    private get entity() {
        return getRepository(User);
    }

    constructor() {
        this.authService = new AuthService();
    }

    async getAll(): Promise<IUserResponse> {
        const users = await this.entity.find();
        if (users.length === 0) {
            return errorResponse(httpStatus.NOT_FOUND, Message.queryError);
        }

        return successResponse('', users);
    }

    async create(data: ICreateUser): Promise<IAuthResponse> {
        let user;
        try {
            data.password = bcrypt.hashSync(data.password, 4);
            const userData = this.entity.create(data);
            user = await this.entity.save(userData);
        } catch (error) {
            return errorResponse(httpStatus.INTERNAL_SERVER_ERROR, error.message);
        }

        return successResponse(Message.successRegister, this.authService.buildToken(user), httpStatus.CREATED);
    }

}
