import {getRepository} from "typeorm";
import {User} from "../entities/user";
import bcrypt = require("bcrypt");
import jwt = require("jsonwebtoken");
import config from "../config/config";
import {httpStatus} from "../config/http-status";
import {ICreateUser, IUser, IUserResponse} from "../interfaces/user.interface";
import {errorResponse, successResponse} from "./json-api-response.service";
import {Message} from "../config/message";
import {IAuth, IAuthenticate, IAuthResponse} from "../interfaces/auth.interface";

export default class AuthService {


    private get entity() {
        return getRepository(User);
    }

    async authenticate(data: IAuthenticate): Promise<IAuthResponse> {
        const {email, password} = data;
        let getUser: any;
        try {
            getUser = await this.entity.findOne({where: {email}});
            if (!getUser) {
                return errorResponse(httpStatus.UNAUTHORIZED, Message.invalidCredentials);
            }
        } catch (error) {
            return errorResponse(httpStatus.INTERNAL_SERVER_ERROR, error.message);
        }

        const user: User = getUser;
        if (!user.checkIfUnencryptedPasswordIsValid(password)) {
            return errorResponse(httpStatus.UNAUTHORIZED, Message.invalidCredentials);
        }

        return successResponse(Message.authenticatedUser, this.buildToken(user));
    }

    async refreshToken(refreshToken: string): Promise<IAuthResponse> {
        let user: User;
        try {
            const verifyRefresh: any = jwt.verify(refreshToken, config.jwtSecretRefresh);
            const {email} = verifyRefresh;
            user = await this.entity.findOneOrFail({where: {email}});
        } catch (error) {
            return errorResponse(httpStatus.INTERNAL_SERVER_ERROR, error.message);
        }

        return successResponse(Message.refreshToken, this.buildToken(user));
    }

    buildToken(user: User): IAuth {
        // @ts-ignore
        delete user.password;
        const accessToken = jwt.sign({
            userId: user.id,
            email: user.email
        }, config.jwtSecret, {expiresIn: config.expiresIn});
        const refreshToken = jwt.sign({
            userId: user.id,
            email: user.email
        }, config.jwtSecretRefresh, {expiresIn: config.expiresRefreshIn});

        return {
            user: user as IUser,
            auth: {
                accessToken, refreshToken, expiresIn: config.expiresIn, expiresRefreshIn: config.expiresRefreshIn
            }
        } as IAuth
    }

}
