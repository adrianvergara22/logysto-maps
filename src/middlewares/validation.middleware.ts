import {Request, Response, NextFunction} from "express";
import {validationResult} from "express-validator";

export const validationMiddleware = (req: Request, res: Response, next: NextFunction) => {
    let errors: any = validationResult(req);
    if (!errors.isEmpty()) {
        console.log('====================================');
        console.log(errors.errors[0].msg);
        console.log('====================================');
        const messages = errors.errors[0].msg;
        return res.status(400).json({
            code: 400,
            success: true,
            message: messages.hasOwnProperty('message') ? messages.message : messages,
            content: {}
        });
    }
    next();
};
