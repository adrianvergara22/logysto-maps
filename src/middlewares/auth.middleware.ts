import {Request, Response, NextFunction} from "express";
import * as jwt from "jsonwebtoken";
import config from "../config/config";

export const authMiddleware = (req: Request, res: Response, next: NextFunction) => {
    const token = <string>req.get("Authorization");

    try {
        const jwtPayload: any = jwt.verify(token, config.jwtSecret);
        console.log(jwtPayload);
    } catch (error) {
        return res.status(401).json({
            success: false, message: 'El usuario no está autorizado para realizar la acción.'
        })
    }

    next();
};
