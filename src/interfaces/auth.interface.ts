import {IUser} from "./user.interface";


export interface IAuthenticate {
    email: string,
    password: string
}

export interface IToken {
    accessToken: string,
    refreshToken: string,
    expiresIn: string,
    expiresRefreshIn: string
}

export interface IAuth {
    user: IUser,
    auth: IToken
}

export interface IAuthResponse {
    code: number,
    success: boolean,
    message: string,
    content: IAuth
}
