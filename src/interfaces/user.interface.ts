export interface ICreateUser {
    name: string,
    lastName: string,
    email: string,
    password: string,
    phone: string,
    address: string
}

export interface IUser {
    id: number,
    name: string,
    lastName: string,
    email: string,
    password: string,
    phone: string,
    address: string,
    createdAt: Date,
    updatedAt: Date
}

export interface IUserResponse {
    code: number,
    success: boolean,
    message: string,
    content: IUser | IUser[]
}
