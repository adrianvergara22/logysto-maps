export interface IGeolocation {
    latitude: string,
    longitude: string,
    provider: string
}

export interface IGeolocationResponse {
    code: number,
    success: boolean,
    message: string,
    content: IGeolocation
}
