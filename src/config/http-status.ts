

export enum httpStatus {
    OK = 200,
    BAD_REQUEST = 400,
    NOT_FOUND = 404,
    UNAUTHORIZED = 401,
    CREATED = 201,
    INTERNAL_SERVER_ERROR = 500
}
