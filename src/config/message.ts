export enum Message {
    emptyFields = "Verifique que haya diligenciado todos los campos.",
    successRegister = "Datos almacenados exitosamente.",
    registerError = "No se pudieron almacenar los datos, por favor intente nuevamente.",
    authError = "El usuario no está autorizado para realizar la solicitud.",
    queryError = "No se encuentran datos almacenados en el sistema.",
    emptyValue = 'Verifique que haya diligenciado el campo {field}.',
    characterQuantity = 'Verifique que el campo {field} no exceda los {length} caracteres permitidos.',
    characterQuantityRange = 'Verifique que el campo {field} contenga entre {min} y {max} caracteres.',
    emailExists = 'El email que desea registrar ya existe.',
    invalidEmail = 'Verifique que haya diligenciado un e-mail válido.',
    invalidCredentials = 'Credenciales inválidas',
    authenticatedUser = 'Usuario autenticado exitosamente.',
    refreshToken = 'Access token generado exitosamente.',
    locationNotFound = 'No se encontraron coordenadas con la dirección especificada.',
    locationFound = 'Coordenadas localizadas exiosamente.'
}
