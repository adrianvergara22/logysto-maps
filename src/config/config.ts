export default {
    port: process.env.PORT || '3200',

    dbPort: Number(process.env.DB_PORT) || 3306,
    dbUsername: process.env.DB_USERNAME || 'adrian',
    dbPassword: process.env.DB_PASSWORD || 'qaz123',
    dbName: process.env.DB_NAME || 'db_logysto',
    dbHost: process.env.DB_HOST || 'localhost',

    jwtSecret: process.env.JWT_SECRET || 'secret-key-logysto',
    expiresIn: process.env.EXPIRES_IN || '1h',

    jwtSecretRefresh: process.env.JWT_SECRET_REFRESH || 'secret-refresh-key-logysto',
    expiresRefreshIn: process.env.EXPIRES_REFRESH_IN || '2h',

    googleMapsUrl: process.env.GOOGLE_MAPS_URL || 'https://maps.googleapis.com/maps/api',
    googleMapsApiKey: process.env.GOOGLE_MAPS_API_KEY || 'AIzaSyDaOkSnYIzRp-xbUGI4Tu83l5tySyVcW2Y',

    tomtomUrl: process.env.TOMTOM_URL || 'https://api.tomtom.com/search/2',
    tomtomApiKey: process.env.TOMTOM_API_KEY || 'qpfU0TjjqPPClohkaOVcrvwcntqltLAR',

    googleProvider: 'google',
    tomtomProvider: 'tomtom'
}
