import {Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn} from "typeorm";
import * as bcrypt from "bcrypt";

@Entity('users')
export class User {

    @PrimaryGeneratedColumn()
        // @ts-ignore
    id: number;

    @Column("varchar", {length: 70})
        // @ts-ignore
    name: string;

    @Column("varchar", {length: 70, name: 'last_name', nullable: true})
    lastName?: string;

    @Column("varchar", {length: 80, unique: true})
        // @ts-ignore
    email: string;

    @Column("varchar", {length: 80})
        // @ts-ignore
    password: string;

    @Column("varchar", {length: 15, nullable: true})
    phone?: string;

    @Column("varchar", {length: 60, nullable: true})
    address?: string;

    @CreateDateColumn({type: 'timestamp', name: 'created_at'})
        // @ts-ignore
    createdAt: Date;

    @UpdateDateColumn({type: 'timestamp', name: 'updated_at'})
        // @ts-ignore
    updatedAt: Date;

    checkIfUnencryptedPasswordIsValid(unencryptedPassword: string) {
        return bcrypt.compareSync(unencryptedPassword, this.password);
    }

}
