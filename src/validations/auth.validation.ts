import {check} from "express-validator";
import {Message} from "../config/message";


export const validateAuth = [
    check('email')
        .exists().withMessage(Message.emptyValue.replace('{field}', 'email'))
        .isEmail().withMessage({message: Message.invalidEmail}),
    check('password')
        .exists().withMessage(Message.emptyValue.replace('{field}', 'contraseña'))
]
