import {check} from "express-validator";
import {Message} from "../config/message";
import {ValidationService} from "../services/validation.service";
import {getRepository} from "typeorm";
import {User} from "../entities/user";


export const validateCreate = [
    check('name')
        .exists().withMessage(Message.emptyValue.replace('{field}', 'nombre'))
        .isLength({max: 70}).withMessage(ValidationService.replaceAll(Message.characterQuantity, ['{field}', "{length}"], ['nombre', '70'])),
    check('lastName')
        .isLength({max: 70}).withMessage(ValidationService.replaceAll(Message.characterQuantity, ['{field}', "{length}"], ['apellido', '70'])),
    check('phone')
        .isLength({max: 15}).withMessage(ValidationService.replaceAll(Message.characterQuantity, ['{field}', "{length}"], ['teléfono', '15'])),
    check('address')
        .isLength({max: 60}).withMessage(ValidationService.replaceAll(Message.characterQuantity, ['{field}', "{length}"], ['dirección', '60'])),
    check('email')
        .exists().withMessage(Message.emptyValue.replace('{field}', 'email'))
        .isEmail().withMessage({message: Message.invalidEmail})
        .isLength({max: 80}).withMessage(ValidationService.replaceAll(Message.characterQuantity, ['{field}', "{length}"], ['email', '80']))
        .custom(async (value) => {
            const userRepository = getRepository(User);
            const user = await userRepository.findOne({email: value})
            if (user) {
                return Promise.reject(Message.emailExists);
            }
        }),
    check('password')
        .exists().withMessage(Message.emptyValue.replace('{field}', 'contraseña'))
        .isLength({
            min: 6,
            max: 10
        }).withMessage(ValidationService.replaceAll(Message.characterQuantityRange, ['{field}', '{min}', 'max'], ['contraseña', '6', '10']))
        .matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}$/, 'i').withMessage('La contraseña debe tener al menos una mayúscula, una minúscula y un número.')


]
